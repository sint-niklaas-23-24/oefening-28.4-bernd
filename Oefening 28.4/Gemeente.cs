﻿namespace Oefening_28._4
{
    internal class Gemeente
    {
        private string _gemeenteNaam;
        private string _postcode;

        public Gemeente(string postcode, string gemeente)
        {
            Postcode = postcode;
            GemeenteNaam = gemeente;
        }

        public string Postcode
        { get { return _postcode; } set { _postcode = value; } }
        public string GemeenteNaam
        { get { return _gemeenteNaam; } set { _gemeenteNaam = value; } }
        public override string ToString()
        {
            return Postcode + " - " + GemeenteNaam;
        }
        public bool Equals(object obj)
        {
            bool isEquals = false;
            return isEquals;
        }
    }
}
